print("Hello World")
"""
this is a comment
"""
# this is a comment

# naming variables

age=35
middle_initial="C"
print(age)
print(middle_initial)

name1,name2,name3,name4 = "John", "Paul", "George", "Ringo"

print(name1)
print(name2)
print(name3)
print(name4)

# string
full_name="John Doe"
secret_code = "Pa$$word"

# number
num_of_days = 365 # integer
pi_approx = 3.1416 # float
complex_num = 1+5j # complex

# boolean
is_learning = True
is_difficult = False

print("Hi! my name is " + full_name)

# [Section] f strings

print(f"Hi! my name is {full_name}. and my age is {age}")

# print("my age is " + age) result:error
# from int to string
print("my age is " + str(age))

# from string to int
print(age + int("5433"))

# [Section] operators
print(2+10)
print(2-10)
print(2*10)
print(2/10)
print(2%10)
print(2**10)

# [Section] assignment operators
num1 = 3
print(num1)
num1 +=4 # num1 = num1 + 4
print(num1)
num1 -=4
print(num1)
num1 *=4
print(num1)
num1 /=4
print(num1)
num1 %=4
print(num1)

# [section] Comparison operators
# returns boolean value to compare values
print(1=="1") #false
print(1!= "1") #true

#logical operators
print(True and False)
print(True or False)
print(not False)





































